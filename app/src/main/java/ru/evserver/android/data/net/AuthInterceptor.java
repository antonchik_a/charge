package ru.evserver.android.data.net;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.evserver.android.AppData;

public class AuthInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder();
        if(AppData.getInstance().getToken() != null) {
            requestBuilder.header("Authorization","Bearer " + AppData.getInstance().getToken());
        }
//        requestBuilder.header("Accept", "application/json")
//                .method(original.method(), original.body());

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }

}
