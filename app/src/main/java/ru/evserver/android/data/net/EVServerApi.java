package ru.evserver.android.data.net;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Observable;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import ru.evserver.android.models.PinConfirmBody;
import ru.evserver.android.models.Price;
import ru.evserver.android.models.Station;
import ru.evserver.android.models.TokenResponse;

public interface EVServerApi {

    @FormUrlEncoded
    @POST("/api/mobile/pin")
    Single<ApiResponse> pin(@NonNull @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/api/mobile/pin_confirm")
    Single<ApiResponse> pinConfirm(@NonNull @Field("phone") String phone, @NonNull @Field("code") String code);

    @FormUrlEncoded
    @POST("/oauth/v2/token")
    Single<TokenResponse> getToken(@NonNull @Field("grant_type") String grant_type,
                                                @NonNull @Field("client_id") String client_id,
                                                @NonNull @Field("client_secret") String client_secret,
                                                @NonNull @Field("username") String phone,
                                                @NonNull @Field("password") String code);

    @GET("/api/mobile/stations")
    Single<ApiResponse<List<Station>>> getStations();

    @FormUrlEncoded
    @POST("/api/mobile/charge_price_anticipate")
    Single<ApiResponse<Price>> getPrice(@NonNull @Field("stationId") String stationId,
                                        @NonNull @Field("expirationTime") String expirationTime);
}
