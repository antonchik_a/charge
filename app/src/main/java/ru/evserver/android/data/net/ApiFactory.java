package ru.evserver.android.data.net;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.evserver.android.Config;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public final class ApiFactory {

    private static OkHttpClient sClient;
    private static volatile EVServerApi sService;

    private ApiFactory() {
    }

    private static final HttpLoggingInterceptor LOGGING_INTERCEPTOR = new HttpLoggingInterceptor();

    static {
        LOGGING_INTERCEPTOR.setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @NonNull
    public static EVServerApi getService() {
        EVServerApi service = sService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sService;
                if (service == null) {
                    service = sService = buildRetrofit().create(EVServerApi.class);
                }
            }
        }
        return service;
    }

    @NonNull
    private static Retrofit buildRetrofit() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        GsonConverterFactory gsonConverterFactory =  GsonConverterFactory.create(gson);

        return new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .client(getClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor())
                //.addInterceptor(new AuthInterceptor())
                .addInterceptor(LOGGING_INTERCEPTOR)
                .addInterceptor(new AuthInterceptor())
                .build();
    }


}

