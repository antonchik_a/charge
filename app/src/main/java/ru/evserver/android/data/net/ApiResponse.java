package ru.evserver.android.data.net;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<T> {

    @SerializedName("data")
    private T data;

    @SerializedName("status_code")
    private int status_code;

    @SerializedName("error")
    private ru.evserver.android.models.Error error;


    public T getData() {
        return data;
    }

    public int getStatus_code() {
        return status_code;
    }

    public ru.evserver.android.models.Error getError() {
        return error;
    }

    public boolean isSuccess(){
        return status_code == 200;
    }
}
