package ru.evserver.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.evserver.android.R;
import ru.evserver.android.data.net.ApiFactory;
import ru.evserver.android.models.Station;

public class DetailStationPriceActivity extends BaseActivity {


    public static void start(StationsActivity stationsActivity, Station data) {
        Intent intent = new Intent(stationsActivity, DetailStationPriceActivity.class);
        intent.putExtra("station", data.getId());
        stationsActivity.startActivity(intent);
    }


    private int mStationId;
    private SeekBar mSeekBar;
    private TextView mValue;
    private TextView mPrice;
    private Button mCalcButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_detail);

        getSupportActionBar().setTitle("Price");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent().hasExtra("station")){
            mStationId = getIntent().getIntExtra("station", 0);
        }else {
            finish();
        }

        mSeekBar = findViewById(R.id.seekBar);
        mSeekBar.setMax(100);
        mValue = findViewById(R.id.value);
        mPrice = findViewById(R.id.price);
        mCalcButton = findViewById(R.id.calc);


        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mValue.setText(String.valueOf(i) + " min");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mCalcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcPrice();
            }
        });

    }

    private void calcPrice() {
        Disposable disposable = ApiFactory.getService().getPrice(String.valueOf(mStationId), String.valueOf(mSeekBar.getProgress()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(priceApiResponse -> {
                    if(priceApiResponse.isSuccess()){
                        mPrice.setText(String.valueOf(priceApiResponse.getData().getCost()));
                    }else {
                        showErrorMessage(priceApiResponse.getError().getMessage());
                    }
                }, throwable -> {
                    showErrorMessage(throwable.getMessage());
                });
        addDisposable(disposable);
    }
}
