package ru.evserver.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.widget.ImageView;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import java.util.ArrayList;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.evserver.android.AppData;
import ru.evserver.android.R;
import ru.evserver.android.data.net.ApiFactory;
import ru.evserver.android.models.Station;

public class StationsActivity extends BaseActivity {

    public static void start(Context context) {
        Intent intent = new Intent(context, StationsActivity.class);
        context.startActivity(intent);
    }


    private RecyclerView mRecyclerView;
    private SlimAdapter mSlimAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stations);

        getSupportActionBar().setTitle("Stations");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mSlimAdapter = SlimAdapter.create()
                .register(R.layout.item_station, new SlimInjector<Station>() {
                    @Override
                    public void onInject(Station data, IViewInjector injector) {
                        injector.text(R.id.title, data.getName())
                                .text(R.id.identity, data.getCharge_box_identity())
                                .clicked(R.id.root, view12 -> {
                                    DetailStationPriceActivity.start(StationsActivity.this, data);
                                });
                    }
                })
                .registerDefault(R.layout.item_loading, new SlimInjector() {
                    @Override
                    public void onInject(Object data, IViewInjector injector) {

                    }
                })
                .attachTo(mRecyclerView);

        ArrayList<String> loading = new ArrayList<>();
        loading.add("Loading");
        mSlimAdapter.updateData(loading);

        loadStations();
    }

    private void loadStations() {

        Disposable disposable = ApiFactory.getService().getStations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listApiResponse -> {
                    if(listApiResponse.isSuccess()){
                        mSlimAdapter.updateData(listApiResponse.getData());
                    }else {
                        showErrorMessage(listApiResponse.getError().getMessage());
                    }

                },throwable -> {
                    showErrorMessage(throwable.getMessage());
                });
        addDisposable(disposable);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppData.getInstance().setToken(null);
    }
}
