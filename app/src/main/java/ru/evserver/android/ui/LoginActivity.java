package ru.evserver.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.evserver.android.AppData;
import ru.evserver.android.Config;
import ru.evserver.android.R;
import ru.evserver.android.data.net.ApiFactory;
import ru.evserver.android.models.PinConfirmBody;

public class LoginActivity extends BaseActivity {


    public static void start(Context context){
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    private EditText mPhoneEdit;
    private EditText mCodeEdit;
    private Button mLoginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPhoneEdit = findViewById(R.id.phone_edit);
        mCodeEdit = findViewById(R.id.pin_code_edit);
        mLoginButton = findViewById(R.id.login);

        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryLogin();
            }
        });
    }

    private void tryLogin() {

        String number = mPhoneEdit.getText().toString();

        if(number.length() == 0){
            showErrorMessage("Please input phone number");
            return;
        }

        String code = mCodeEdit.getText().toString();

        if(code.length() == 0){
            showErrorMessage("Please input code");
            return;
        }

        getToken(number, code);

//        PinConfirmBody pinConfirmBody = new PinConfirmBody(number, code);
//
//        Disposable disposable = ApiFactory.getService().pinConfirm(number, code)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(apiResponse -> {
//                    if(apiResponse.isSuccess()){
//                        getToken(number, code);
//                    }else {
//                        showErrorMessage(apiResponse.getError().getMessage());
//                    }
//                }, throwable -> {
//                    showErrorMessage("Error! Try again");
//                });
//        addDisposable(disposable);
    }

    private void getToken(String number, String code){
        Disposable disposable = ApiFactory.getService().getToken(Config.GRANT_TYPE, Config.CLIENT_ID, Config.CLIENT_SECRET, number, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tokenResponseApiResponse -> {
                        AppData.getInstance().setToken(tokenResponseApiResponse.getAccessToken());
                        StationsActivity.start(LoginActivity.this);

                },throwable -> {
                    if(throwable.getMessage().contains("Bad Request")){
                        showErrorMessage("Invalid credentials");
                    }else{
                        showErrorMessage(throwable.getMessage());
                    }
                });

        addDisposable(disposable);
    }

}
