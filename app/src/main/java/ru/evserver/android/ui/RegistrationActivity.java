package ru.evserver.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.evserver.android.R;
import ru.evserver.android.data.net.ApiFactory;

public class RegistrationActivity extends BaseActivity {

    public static void start(Context context){
        Intent intent = new Intent(context, RegistrationActivity.class);
        context.startActivity(intent);
    }

    private EditText mPhoneEdit;
    private EditText mCodeEdit;
    private TextInputLayout mTextInputLayout;
    private Button mRegButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mPhoneEdit = findViewById(R.id.phone_edit);
        mCodeEdit = findViewById(R.id.pin_code_edit);
        mTextInputLayout = findViewById(R.id.pin_layout);
        mRegButton = findViewById(R.id.registration);

        mTextInputLayout.setVisibility(View.GONE);
        getSupportActionBar().setTitle("Registration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRegButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mTextInputLayout.getVisibility() == View.GONE) {
                    registration();
                }else {
                    confirm();
                }
            }
        });

    }

    private void confirm() {
        String number = mPhoneEdit.getText().toString();

        if(number.length() == 0){
            showErrorMessage("Please input phone number");
            return;
        }

        String code = mCodeEdit.getText().toString();

        if(code.length() == 0){
            showErrorMessage("Please input code");
            return;
        }


        Disposable disposable = ApiFactory.getService().pinConfirm(number, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(apiResponse -> {
                    if(apiResponse.isSuccess()){
                        showErrorMessage("Success confirm!");
                        finish();
                    }else {
                        showErrorMessage(apiResponse.getError().getMessage());
                    }
                }, throwable -> {
                    showErrorMessage("Error! Try again");
                });
        addDisposable(disposable);
    }

    private void registration() {

        String number = mPhoneEdit.getText().toString();

        if(number.length() == 0){
            showErrorMessage("Please input phone number");
            return;
        }


        Disposable disposable = ApiFactory.getService().pin(number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(apiResponse -> {
                    if(apiResponse.isSuccess()){
                        showErrorMessage("Waiting SMS with code");
                        mTextInputLayout.setVisibility(View.VISIBLE);
                        mRegButton.setText("Confirm");
                    }else {
                        showErrorMessage(apiResponse.getError().getMessage());
                    }
                }, throwable -> {
                    showErrorMessage("Error! Try again");
                });
        addDisposable(disposable);

    }
}
