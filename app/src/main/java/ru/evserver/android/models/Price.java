package ru.evserver.android.models;

import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("cost")
    private double cost;

    public double getCost() {
        return cost;
    }
}
