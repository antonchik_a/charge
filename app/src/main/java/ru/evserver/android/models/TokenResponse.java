package ru.evserver.android.models;

import com.google.gson.annotations.SerializedName;

public class TokenResponse {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("expires_in")
    private int expires_id;

    @SerializedName("refresh_token")
    private String refreshToken;

    public String getAccessToken() {
        return accessToken;
    }

    public int getExpires_id() {
        return expires_id;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
