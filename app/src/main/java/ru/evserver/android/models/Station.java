package ru.evserver.android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Station implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("name_ru")
    private String name;

    @SerializedName("charge_box_identity")
    private String charge_box_identity;

    public String getCharge_box_identity() {
        return charge_box_identity;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
