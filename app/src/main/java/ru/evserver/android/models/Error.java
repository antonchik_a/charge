package ru.evserver.android.models;

import com.google.gson.annotations.SerializedName;

public class Error {

    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("message_ru")
    private String messageRu;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageRu() {
        return messageRu;
    }
}
