package ru.evserver.android.models;

import com.google.gson.annotations.SerializedName;

public class PinConfirmBody {

    public PinConfirmBody(String phone, String code) {
        this.phone = phone;
        this.code = code;
    }

    @SerializedName("phone")
    private String phone;

    @SerializedName("code")
    private String code;

    public String getPhone() {
        return phone;
    }

    public String getCode() {
        return code;
    }
}
