package ru.evserver.android;

public class AppData {


    private static volatile AppData INSTANCE;

    private String token = null;

    public static AppData getInstance() {

        AppData localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (AppData.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    INSTANCE = localInstance = new AppData();
                }
            }
        }
        return localInstance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
